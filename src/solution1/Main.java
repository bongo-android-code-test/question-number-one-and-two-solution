package solution1;

public class Main {

    public static void main(String[] args) {

        String string1 = "bleat";
        String string2 = "table";
        if (Operations.areAnagram(string1, string2))
            System.out.println(string1 + " and " + string2 + " are anagram of each other");
        else
            System.out.println(string1 + " and " + string2 + " are not anagram of each other");


        String string3 = "eat";
        String string4 = "tar";
        if (Operations.areAnagram(string3, string4))
            System.out.println(string3 + " and " + string4 + " are anagram of each other");
        else
            System.out.println(string3 + " and " + string4 + " are not anagram of each other");


    }
}
