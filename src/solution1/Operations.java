package solution1;

public class Operations {

    /**
     * Method to check whether two strings are anagram
     *
     * @param string1 first string
     * @param string2 second string
     * @return {@code true} if the given strings are anagram, {@code false} otherwise
     */

    static boolean areAnagram(String string1, String string2) {
        // If two strings have different length
        if (string1.length() != string2.length()) {
            return false;
        }

        // To store the xor value
        int value = 0;

        for (int i = 0; i < string1.length(); i++) {
            value = value ^ (int) string1.charAt(i);
            value = value ^ (int) string2.charAt(i);
        }

        return value == 0;
    }
}
