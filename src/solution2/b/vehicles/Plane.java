package solution2.b.vehicles;

import solution2.b.interfaces.PlaneInterface;

public class Plane implements PlaneInterface {

    private static final int topVelocity = 500;
    private static final int maxAltitude = 1000;
    private static final int minAltitude = 200;

    private int velocity;
    private int fuel;
    private int altitude;
    private String colour;
    private String modelNumber;

    public Plane(int velocity, int fuel, int altitude, String colour, String modelNumber) {
        this.velocity = velocity;
        this.fuel = fuel;
        this.altitude = altitude;
        this.colour = colour;
        this.modelNumber = modelNumber;
    }

    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    @Override
    public void fly() {
        System.out.println("Plane is flying");
        altitude = minAltitude;
    }

    @Override
    public void land() {
        System.out.println("Plane not flying");
        altitude = 0;
        velocity = 0;
    }

    @Override
    public void flyHigher() {
        System.out.println("Plane fly Higher");
        altitude += 10;
        if (altitude > maxAltitude) {
            altitude = maxAltitude;
        }
    }

    @Override
    public void flyLower() {
        System.out.println("Plane fly Lower");
        altitude -= 10;
        if (altitude < minAltitude) {
            altitude = minAltitude;
        }
    }

    @Override
    public void velocityUp() {
        System.out.println("Velocity Up");
        velocity += 10;
        if (velocity > topVelocity) {
            velocity = topVelocity;
        }

    }

    @Override
    public void velocityDown() {
        System.out.println("Velocity Down");
        velocity -= 10;
        if (velocity < 0) {
            velocity = 0;
        }
    }

    @Override
    public boolean hasFuel() {
        if (fuel == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void start() {
        System.out.println("Plane start");
        velocity = 0;
        altitude = 0;
    }

    @Override
    public void stop() {
        System.out.println("Plane stop");
        velocity = 0;
        altitude = 0;
    }

    @Override
    public void move() {
        velocity = 1;
        altitude = 0;
    }


    @Override
    public String getColor() {
        return colour;
    }

    @Override
    public String getModelNumbers() {
        return modelNumber;
    }

    @Override
    public String getCurrentStatus() {
        return "Plane{" +
                "Velocity = " + velocity +
                ", Fuel = " + fuel +
                ", Altitude = " + altitude +
                ", ModelNumber = " + modelNumber +
                ", Colour = " + colour +
                '}';
    }
}
