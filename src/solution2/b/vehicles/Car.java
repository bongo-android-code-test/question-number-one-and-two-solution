package solution2.b.vehicles;

import solution2.b.interfaces.CarInterface;

public class Car implements CarInterface {

    private static final int maxVelocity = 200;
    private static final int maxGear = 5;

    private int velocity;
    private int gear;
    private int fuel;
    private String colour;
    private String modelNumber;

    public Car(int velocity, int gear, int fuel, String colour, String modelNumber) {
        this.velocity = velocity;
        this.gear = gear;
        this.fuel = fuel;
        this.colour = colour;
        this.modelNumber = modelNumber;
    }
    public int getVelocity() {
        return velocity;
    }

    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    @Override
    public void gearUp() {
        System.out.println("Gear Up");
        if (gear < maxGear) {
            velocityUp();
            gear++;
        }
    }

    @Override
    public void gearDown() {
        System.out.println("Gear Down");
        if (gear > 0) {
            velocityDown();
            gear--;
        }
    }


    @Override
    public void velocityUp() {
        velocity += 10;
        if (velocity > maxVelocity) {
            velocity = maxVelocity;
        }
    }

    @Override
    public void velocityDown() {
        velocity -= 10;
        if (velocity < 0) {
            velocity = 0;
        }
    }

    @Override
    public boolean hasFuel() {
        if (fuel == 0) {
            return false;
        }
        return true;
    }

    @Override
    public void start() {
        System.out.println("Car Start");
        gear = 0;
        velocity = 0;
    }

    @Override
    public void stop() {
        System.out.println("Car Stop");
        gear = 0;
        velocity = 0;
    }

    @Override
    public void move() {
        gear = 1;
        velocity = 1;
    }

    @Override
    public String getColor() {
        return colour;
    }

    @Override
    public String getModelNumbers() {
        return modelNumber;
    }

    @Override
    public String getCurrentStatus() {
        return "Car {" +
                "Velocity = " + velocity +
                ", Gear = " + gear +
                ", Fuel = " + fuel +
                ", ModelNumber = " + modelNumber +
                ", Colour = " + colour +
                '}';
    }
}
