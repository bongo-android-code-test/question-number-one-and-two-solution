package solution2.b.interfaces;

public interface Vehicle {
    void velocityUp();
    void velocityDown();
    boolean hasFuel();
    void start();
    void stop();
    void move();
    String getColor();
    String getModelNumbers();
    String getCurrentStatus();

}
