package solution2.b.interfaces;

public interface PlaneInterface extends Vehicle{
    void fly();
    void land();
    void flyHigher();
    void flyLower();
}
