package solution2.b.interfaces;

public interface CarInterface extends Vehicle {

    void gearUp();
    void gearDown();
}
