package solution2.b;

import solution2.b.vehicles.Car;
import solution2.b.vehicles.Plane;

public class Main {

    public static void main(String[] args) {
        Car car = new Car(0,0,75, "Red","Lamborghini Aventador LP 7" );
        car.start();
        car.gearUp();
        car.move();
        System.out.println(car.getCurrentStatus());
        car.setFuel(70);
        car.gearUp();
        System.out.println(car.getCurrentStatus());
        car.setFuel(60);
        car.gearDown();
        System.out.println(car.getCurrentStatus());
        car.setFuel(50);
        car.stop();
        System.out.println(car.getCurrentStatus());

        System.out.println("\n---------------------\n");

        Plane plane = new Plane(0,10000, 5000, "White", "Boeing 777");
        plane.start();
        plane.move();
        plane.fly();
        System.out.println(plane.getCurrentStatus());
        plane.setFuel(7000);
        plane.flyHigher();
        System.out.println(plane.getCurrentStatus());
        plane.setFuel(6000);
        plane.velocityUp();
        System.out.println(plane.getCurrentStatus());
        plane.setFuel(5000);
        plane.land();
        System.out.println(plane.getCurrentStatus());
        plane.stop();
        System.out.println(plane.getCurrentStatus());
    }
}
