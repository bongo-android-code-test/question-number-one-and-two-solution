package solution2.a;

public class Plane implements Vehicle {

    private String manufacturer;
    private float velocity;
    private float altitude;
    private int numOfSeats;

    private int numOfWheels;
    private int numOfPassengers;
    private boolean hasGas;


    public Plane(int numOfWheels, int numOfPassengers, boolean hasGas) {
        this.numOfWheels = numOfWheels;
        this.numOfPassengers = numOfPassengers;
        this.hasGas = hasGas;
    }

    public Plane(String manufacturer, float velocity, float altitude, int numOfSeats, int numOfWheels, int numOfPassengers, boolean hasGas) {
        this.manufacturer = manufacturer;
        this.velocity = velocity;
        this.altitude = altitude;
        this.numOfSeats = numOfSeats;
        this.numOfWheels = numOfWheels;
        this.numOfPassengers = numOfPassengers;
        this.hasGas = hasGas;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public int getNumOfWheels() {
        return numOfWheels;
    }

    public int getNumOfPassengers() {
        return numOfPassengers;
    }


    public void setHasGas(boolean hasGas) {
        this.hasGas = hasGas;
    }

    @Override
    public void setNumOfWheels(int wheels) {
        this.numOfWheels = wheels;
    }

    @Override
    public void setNumOfPassengers(int passengers) {
        this.numOfPassengers = passengers;
    }

    @Override
    public boolean hasGas() {
        return hasGas;
    }
}
