package solution2.a;

import solution1.Operations;

public class Main {

    public static void main(String[] args) {

        /*................... Car ........................*/

        Car car = new Car("Toyota", 122, 4, 4, 2, true);
        System.out.println("\n\nCar Properties ");
        System.out.println("manufacturer : " + car.getManufacturer());
        System.out.println("velocity : " + car.getVelocity());
        System.out.println("numOfSeats : " + car.getNumOfSeats());
        System.out.println("numOfWheels : " + car.getNumOfWheels());
        System.out.println("numOfPassengers : " + car.getNumOfPassengers());
        System.out.println("hasGas : " + car.hasGas());


        /*................... Plane ........................*/
        Plane plane = new Plane("Embraer", 122, 2000, 4, 4, 2, true);
        System.out.println("\n\n\nPlane Properties ");
        System.out.println("manufacturer : " + plane.getManufacturer());
        System.out.println("altitude : " + plane.getManufacturer());
        System.out.println("velocity : " + plane.getVelocity());
        System.out.println("numOfSeats : " + plane.getNumOfSeats());
        System.out.println("numOfWheels : " + plane.getNumOfWheels());
        System.out.println("numOfPassengers : " + plane.getNumOfPassengers());
        System.out.println("hasGas : " + plane.hasGas());

    }
}
