package solution2.a;

public class Car implements Vehicle{
    private String manufacturer;
    private float velocity ;
    private int numOfSeats;

    private int numOfWheels;
    private int numOfPassengers;
    private boolean hasGas;

    public Car(int numOfWheels, int numOfPassengers, boolean hasGas) {
        setNumOfWheels(numOfWheels);
        setNumOfPassengers(numOfPassengers);
        setHasGas(hasGas);
    }

    public Car(String manufacturer, float velocity, int numOfSeats, int numOfWheels, int numOfPassengers, boolean hasGas) {
        setManufacturer(manufacturer);
        setVelocity(velocity);
        setNumOfSeats(numOfSeats);
        setNumOfWheels(numOfWheels);
        setNumOfPassengers(numOfPassengers);
        setHasGas(hasGas);
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public float getVelocity() {
        return velocity;
    }

    public void setVelocity(float velocity) {
        this.velocity = velocity;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public int getNumOfWheels() {
        return numOfWheels;
    }
    @Override
    public void setNumOfWheels(int numOfWheels) {
        this.numOfWheels = numOfWheels;
    }

    public int getNumOfPassengers() {
        return numOfPassengers;
    }
    @Override
    public void setNumOfPassengers(int numOfPassengers) {
        this.numOfPassengers = numOfPassengers;
    }

    @Override
    public boolean hasGas() {
        return hasGas;
    }

    public void setHasGas(boolean hasGas) {
        this.hasGas = hasGas;
    }

}
