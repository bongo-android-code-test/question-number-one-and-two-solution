package solution2.a;

public interface Vehicle {
    void setNumOfWheels(int wheels);
    void setNumOfPassengers(int passengers);
    boolean hasGas();
}
